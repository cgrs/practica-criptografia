﻿/*
 * 
 */
 
using System;
using System.Numerics;
using System.Windows.Forms;
using System.Collections;

namespace Criptografia
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
			this.ActiveControl = euclidesA;
		}
		
		void ClickEventHandler(object sender, EventArgs e)
		{
			try {
				if (sender.Equals(buttonEuclides)) {
					long a = long.Parse(euclidesA.Text);
					long b = long.Parse(euclidesB.Text);
					Euclides(a,b);
				}
				else if (sender.Equals(buttonBezout)) {
					long a = long.Parse(bezoutA.Text);
					long b = long.Parse(bezoutB.Text);
					Bezout(a,b);
				}
				else if (sender.Equals(buttonCuadrados)) {
					BigInteger a = BigInteger.Parse(cuadradosA.Text);
					BigInteger ex = BigInteger.Parse(cuadradosE.Text);
					BigInteger n = BigInteger.Parse(cuadradosN.Text);
					Cuadrados(a,ex,n);
				}
				else if (sender.Equals(buttonInverso)) {
					long a = long.Parse(inversoA.Text);
					long n = long.Parse(inversoN.Text);
					Inverso(a,n);
				}
				else if (sender.Equals(buttonRestos)) {
					ArrayList a = new ArrayList();
					
					if (!string.IsNullOrEmpty(restosA1.Text)) a.Add(long.Parse(restosA1.Text));
					if (!string.IsNullOrEmpty(restosA2.Text)) a.Add(long.Parse(restosA2.Text));
					if (!string.IsNullOrEmpty(restosA3.Text)) a.Add(long.Parse(restosA3.Text));
					if (!string.IsNullOrEmpty(restosA4.Text)) a.Add(long.Parse(restosA4.Text));
					if (!string.IsNullOrEmpty(restosA5.Text)) a.Add(long.Parse(restosA5.Text));
					if (!string.IsNullOrEmpty(restosA6.Text)) a.Add(long.Parse(restosA6.Text));
					
					ArrayList n = new ArrayList();
					
					if (!string.IsNullOrEmpty(restosN1.Text)) n.Add(long.Parse(restosN1.Text));
					if (!string.IsNullOrEmpty(restosN2.Text)) n.Add(long.Parse(restosN2.Text));
					if (!string.IsNullOrEmpty(restosN3.Text)) n.Add(long.Parse(restosN3.Text));
					if (!string.IsNullOrEmpty(restosN4.Text)) n.Add(long.Parse(restosN4.Text));
					if (!string.IsNullOrEmpty(restosN5.Text)) n.Add(long.Parse(restosN5.Text));
					if (!string.IsNullOrEmpty(restosN6.Text)) n.Add(long.Parse(restosN6.Text));
					
					Restos(a,n);
				}
			} catch (Exception ex) {
				logBox.AppendText(String.Format("Error: {0}",ex.Message) + Environment.NewLine);
			}
		}
		
		void Euclides(long a, long b)
		{
				Euclides e = new Criptografia.Euclides(a,b);
				String resultado = String.Format("mcd({0}, {1}) = {2}", a, b, e.mcd);
				logBox.AppendText(resultado + Environment.NewLine);
		}
		
		void Bezout(long a, long b)
		{
			Euclides e = new Criptografia.Euclides(a,b);
			String resultado = String.Format("{0}*({1}) + {2}*({3}) = {4}", a, e.x, b, e.y, e.mcd);
			logBox.AppendText(resultado + Environment.NewLine);
		}
		
		void Cuadrados(BigInteger a, BigInteger e, BigInteger n)
		{
			Cuadrados c = new Criptografia.Cuadrados(a,e,n);
			String resultado = String.Format("{0}≡{1}^{2} mod {3}",c.b, a,e,n);
			logBox.AppendText(resultado + Environment.NewLine);
		}
		
		void Inverso(long a, long n)
		{
			Inverso i = new Criptografia.Inverso(a,n);
			String resultado = String.Format("{0}≡{1}^-1 mod {2}",i.b, a,n);
			logBox.AppendText(resultado + Environment.NewLine);
		}
		
		void Restos(ArrayList a, ArrayList n)
		{
			Restos r = new Criptografia.Restos((long []) a.ToArray(typeof(long)), (long []) n.ToArray(typeof(long)));
			for(int i=0;i<a.Count;++i) {
				logBox.AppendText(String.Format("{0}≡{1} mod {2}", r.x, a[i], n[i]) + Environment.NewLine);
			}
		}
	}
}
