﻿/*
 *
 * Usuario: cgrs
 * Fecha: 23/03/2017
 * Hora: 20:09
 * 
 */
using System;

namespace Criptografia
{
	public class Inverso
	{
		public long b;
		
		public Inverso(long a, long n)
		{		
			var egcd = calcular(a,n);
			
			if(egcd.Item3 == 1) {
				b = (egcd.Item1%n + n) % n;
			}
			else throw new Exception("No existe inverso");
		}
		
		Tuple<long,long,long> calcular(long x, long y)
		{
			if (y == 0) {
				return Tuple.Create((long) 1, (long) 0, x);
			}
			var q = calcular(y, x%y);
			
			var d1 = q.Item3;
			var s1 = q.Item1;
			var t1 = q.Item2;
			
			var d = d1;
			var s = t1;
			var t = s1 - (x/y)*t1;
			
            return Tuple.Create(s,t,d);
		}
	}
}
