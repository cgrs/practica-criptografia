﻿/*
 * Usuario: cgrs
 * Fecha: 27/02/2017
 * Hora: 20:38
 * 
 */
using System;

namespace Criptografia
{
	public class Euclides
	{
		public long mcd;
		public long x;
		public long y;
		
		public Euclides(long a, long b)
		{
			if (a >= b && b > 0) {
				var resultado = calcular(a,b);
				mcd = resultado.Item3;
				x = resultado.Item1;
				y = resultado.Item2;
			} else if (b == 0) {
				throw new Exception("El segundo operando debe ser mayor de cero");
			} else if (a < b) {
				throw new Exception("El primer operando debe ser mayor que el segundo");
			}
		}
		
		Tuple<long,long,long> calcular(long a, long b)
		{
			if (b == 0) {
				return Tuple.Create((long) 1, (long) 0, a);
			}
			var q = calcular(b, a%b);
			
			var d1 = q.Item3;
			var s1 = q.Item1;
			var t1 = q.Item2;
			
			var d = d1;
			var s = t1;
			var t = s1 - (a/b)*t1;
			
            return Tuple.Create(s,t,d);
		}
	}
}
