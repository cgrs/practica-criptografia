﻿/*
 *
 * Usuario: cgrs
 * Fecha: 23/03/2017
 * Hora: 21:36
 * 
 */
using System;

namespace Criptografia
{
	
	public class Restos
	{
		public long x;
		
		public Restos(long[] a, long[] n)
		{
			if (a.Length != n.Length)
				throw new Exception("No hay el mismo número de elementos");
			
			long prod = 1,
			sum = 0,
			p;
			
			for (int i = 0; i<n.Length;++i) prod *= n[i];
			for (int i = 0; i<a.Length;++i) {
				p = prod / n[i];
				var inv = new Inverso(p, n[i]).b;
				sum += a[i] * inv * p;
			}
			x = sum % prod;
		}
	}
}
