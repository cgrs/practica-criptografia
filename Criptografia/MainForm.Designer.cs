﻿/*
 * 
 */
namespace Criptografia
{
	partial class MainForm
	{
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.TextBox euclidesA;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox euclidesB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox logBox;
		private System.Windows.Forms.Button buttonEuclides;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button buttonBezout;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox bezoutB;
		private System.Windows.Forms.TextBox bezoutA;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button buttonCuadrados;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox cuadradosE;
		private System.Windows.Forms.TextBox cuadradosA;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox cuadradosN;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox inversoN;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Button buttonInverso;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox inversoA;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Button buttonRestos;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox restosN6;
		private System.Windows.Forms.TextBox restosN5;
		private System.Windows.Forms.TextBox restosN4;
		private System.Windows.Forms.TextBox restosN3;
		private System.Windows.Forms.TextBox restosN2;
		private System.Windows.Forms.TextBox restosN1;
		private System.Windows.Forms.TextBox restosA6;
		private System.Windows.Forms.TextBox restosA5;
		private System.Windows.Forms.TextBox restosA4;
		private System.Windows.Forms.TextBox restosA3;
		private System.Windows.Forms.TextBox restosA2;
		private System.Windows.Forms.TextBox restosA1;
		
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonEuclides = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.euclidesB = new System.Windows.Forms.TextBox();
			this.euclidesA = new System.Windows.Forms.TextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.label5 = new System.Windows.Forms.Label();
			this.buttonBezout = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.bezoutB = new System.Windows.Forms.TextBox();
			this.bezoutA = new System.Windows.Forms.TextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.label13 = new System.Windows.Forms.Label();
			this.cuadradosN = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.buttonCuadrados = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.cuadradosE = new System.Windows.Forms.TextBox();
			this.cuadradosA = new System.Windows.Forms.TextBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.label14 = new System.Windows.Forms.Label();
			this.inversoN = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.buttonInverso = new System.Windows.Forms.Button();
			this.label16 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.inversoA = new System.Windows.Forms.TextBox();
			this.logBox = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label19 = new System.Windows.Forms.Label();
			this.restosA1 = new System.Windows.Forms.TextBox();
			this.restosA2 = new System.Windows.Forms.TextBox();
			this.restosA3 = new System.Windows.Forms.TextBox();
			this.restosA6 = new System.Windows.Forms.TextBox();
			this.restosA5 = new System.Windows.Forms.TextBox();
			this.restosA4 = new System.Windows.Forms.TextBox();
			this.restosN6 = new System.Windows.Forms.TextBox();
			this.restosN5 = new System.Windows.Forms.TextBox();
			this.restosN4 = new System.Windows.Forms.TextBox();
			this.restosN3 = new System.Windows.Forms.TextBox();
			this.restosN2 = new System.Windows.Forms.TextBox();
			this.restosN1 = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.buttonRestos = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(3, 3);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(344, 272);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Controls.Add(this.buttonEuclides);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.euclidesB);
			this.tabPage1.Controls.Add(this.euclidesA);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(335, 245);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Euclides";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(64, 40);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(200, 32);
			this.label4.TabIndex = 6;
			this.label4.Text = "mcd(a, b) = mcd(b, r)";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonEuclides
			// 
			this.buttonEuclides.Location = new System.Drawing.Point(120, 168);
			this.buttonEuclides.Name = "buttonEuclides";
			this.buttonEuclides.Size = new System.Drawing.Size(112, 24);
			this.buttonEuclides.TabIndex = 5;
			this.buttonEuclides.Text = "Calcular";
			this.buttonEuclides.UseVisualStyleBackColor = true;
			this.buttonEuclides.Click += new System.EventHandler(this.ClickEventHandler);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 8);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(320, 32);
			this.label3.TabIndex = 4;
			this.label3.Text = "Algoritmo de Euclides";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "b";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 80);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "a";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// euclidesB
			// 
			this.euclidesB.BackColor = System.Drawing.SystemColors.Window;
			this.euclidesB.Location = new System.Drawing.Point(120, 128);
			this.euclidesB.Name = "euclidesB";
			this.euclidesB.Size = new System.Drawing.Size(112, 20);
			this.euclidesB.TabIndex = 1;
			// 
			// euclidesA
			// 
			this.euclidesA.BackColor = System.Drawing.SystemColors.Window;
			this.euclidesA.Location = new System.Drawing.Point(120, 80);
			this.euclidesA.Name = "euclidesA";
			this.euclidesA.Size = new System.Drawing.Size(112, 20);
			this.euclidesA.TabIndex = 0;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.label5);
			this.tabPage2.Controls.Add(this.buttonBezout);
			this.tabPage2.Controls.Add(this.label6);
			this.tabPage2.Controls.Add(this.label7);
			this.tabPage2.Controls.Add(this.label8);
			this.tabPage2.Controls.Add(this.bezoutB);
			this.tabPage2.Controls.Add(this.bezoutA);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(335, 245);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Bezout";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(64, 40);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(200, 32);
			this.label5.TabIndex = 13;
			this.label5.Text = "ax+by = mcd(a, b)";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonBezout
			// 
			this.buttonBezout.Location = new System.Drawing.Point(120, 168);
			this.buttonBezout.Name = "buttonBezout";
			this.buttonBezout.Size = new System.Drawing.Size(112, 24);
			this.buttonBezout.TabIndex = 12;
			this.buttonBezout.Text = "Calcular";
			this.buttonBezout.UseVisualStyleBackColor = true;
			this.buttonBezout.Click += new System.EventHandler(this.ClickEventHandler);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(8, 8);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(320, 32);
			this.label6.TabIndex = 11;
			this.label6.Text = "Identidad de Bezout";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(24, 128);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 23);
			this.label7.TabIndex = 10;
			this.label7.Text = "b";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(24, 80);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 23);
			this.label8.TabIndex = 9;
			this.label8.Text = "a";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// bezoutB
			// 
			this.bezoutB.BackColor = System.Drawing.SystemColors.Window;
			this.bezoutB.Location = new System.Drawing.Point(120, 128);
			this.bezoutB.Name = "bezoutB";
			this.bezoutB.Size = new System.Drawing.Size(112, 20);
			this.bezoutB.TabIndex = 8;
			// 
			// bezoutA
			// 
			this.bezoutA.BackColor = System.Drawing.SystemColors.Window;
			this.bezoutA.Location = new System.Drawing.Point(120, 80);
			this.bezoutA.Name = "bezoutA";
			this.bezoutA.Size = new System.Drawing.Size(112, 20);
			this.bezoutA.TabIndex = 7;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.label13);
			this.tabPage3.Controls.Add(this.cuadradosN);
			this.tabPage3.Controls.Add(this.label9);
			this.tabPage3.Controls.Add(this.buttonCuadrados);
			this.tabPage3.Controls.Add(this.label10);
			this.tabPage3.Controls.Add(this.label11);
			this.tabPage3.Controls.Add(this.label12);
			this.tabPage3.Controls.Add(this.cuadradosE);
			this.tabPage3.Controls.Add(this.cuadradosA);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(335, 246);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Cuadrados sucesivos";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(24, 136);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(88, 23);
			this.label13.TabIndex = 22;
			this.label13.Text = "n";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cuadradosN
			// 
			this.cuadradosN.BackColor = System.Drawing.SystemColors.Window;
			this.cuadradosN.Location = new System.Drawing.Point(120, 136);
			this.cuadradosN.Name = "cuadradosN";
			this.cuadradosN.Size = new System.Drawing.Size(112, 20);
			this.cuadradosN.TabIndex = 21;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(64, 40);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(200, 32);
			this.label9.TabIndex = 20;
			this.label9.Text = "b ≡ aᵉ (mod n)";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonCuadrados
			// 
			this.buttonCuadrados.Location = new System.Drawing.Point(120, 168);
			this.buttonCuadrados.Name = "buttonCuadrados";
			this.buttonCuadrados.Size = new System.Drawing.Size(112, 24);
			this.buttonCuadrados.TabIndex = 22;
			this.buttonCuadrados.Text = "Calcular";
			this.buttonCuadrados.UseVisualStyleBackColor = true;
			this.buttonCuadrados.Click += new System.EventHandler(this.ClickEventHandler);
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(8, 8);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(320, 32);
			this.label10.TabIndex = 18;
			this.label10.Text = "Cuadrados sucesivos";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(24, 108);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(88, 23);
			this.label11.TabIndex = 17;
			this.label11.Text = "e";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(24, 80);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(88, 23);
			this.label12.TabIndex = 16;
			this.label12.Text = "a";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cuadradosE
			// 
			this.cuadradosE.BackColor = System.Drawing.SystemColors.Window;
			this.cuadradosE.Location = new System.Drawing.Point(120, 108);
			this.cuadradosE.Name = "cuadradosE";
			this.cuadradosE.Size = new System.Drawing.Size(112, 20);
			this.cuadradosE.TabIndex = 15;
			// 
			// cuadradosA
			// 
			this.cuadradosA.BackColor = System.Drawing.SystemColors.Window;
			this.cuadradosA.Location = new System.Drawing.Point(120, 80);
			this.cuadradosA.Name = "cuadradosA";
			this.cuadradosA.Size = new System.Drawing.Size(112, 20);
			this.cuadradosA.TabIndex = 14;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.buttonRestos);
			this.tabPage4.Controls.Add(this.label25);
			this.tabPage4.Controls.Add(this.label26);
			this.tabPage4.Controls.Add(this.label27);
			this.tabPage4.Controls.Add(this.label28);
			this.tabPage4.Controls.Add(this.label29);
			this.tabPage4.Controls.Add(this.label30);
			this.tabPage4.Controls.Add(this.label23);
			this.tabPage4.Controls.Add(this.label24);
			this.tabPage4.Controls.Add(this.label21);
			this.tabPage4.Controls.Add(this.label22);
			this.tabPage4.Controls.Add(this.label20);
			this.tabPage4.Controls.Add(this.label17);
			this.tabPage4.Controls.Add(this.restosN6);
			this.tabPage4.Controls.Add(this.restosN5);
			this.tabPage4.Controls.Add(this.restosN4);
			this.tabPage4.Controls.Add(this.restosN3);
			this.tabPage4.Controls.Add(this.restosN2);
			this.tabPage4.Controls.Add(this.restosN1);
			this.tabPage4.Controls.Add(this.restosA6);
			this.tabPage4.Controls.Add(this.restosA5);
			this.tabPage4.Controls.Add(this.restosA4);
			this.tabPage4.Controls.Add(this.restosA3);
			this.tabPage4.Controls.Add(this.restosA2);
			this.tabPage4.Controls.Add(this.restosA1);
			this.tabPage4.Controls.Add(this.label19);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(336, 246);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Teorema chino";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.label14);
			this.tabPage5.Controls.Add(this.inversoN);
			this.tabPage5.Controls.Add(this.label15);
			this.tabPage5.Controls.Add(this.buttonInverso);
			this.tabPage5.Controls.Add(this.label16);
			this.tabPage5.Controls.Add(this.label18);
			this.tabPage5.Controls.Add(this.inversoA);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(335, 245);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "Inverso";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(24, 128);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(88, 23);
			this.label14.TabIndex = 31;
			this.label14.Text = "n";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// inversoN
			// 
			this.inversoN.BackColor = System.Drawing.SystemColors.Window;
			this.inversoN.Location = new System.Drawing.Point(120, 128);
			this.inversoN.Name = "inversoN";
			this.inversoN.Size = new System.Drawing.Size(112, 20);
			this.inversoN.TabIndex = 24;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(64, 40);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(200, 32);
			this.label15.TabIndex = 29;
			this.label15.Text = "b ≡ aˉ¹ (mod n)";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonInverso
			// 
			this.buttonInverso.Location = new System.Drawing.Point(120, 168);
			this.buttonInverso.Name = "buttonInverso";
			this.buttonInverso.Size = new System.Drawing.Size(112, 24);
			this.buttonInverso.TabIndex = 28;
			this.buttonInverso.Text = "Calcular";
			this.buttonInverso.UseVisualStyleBackColor = true;
			this.buttonInverso.Click += new System.EventHandler(this.ClickEventHandler);
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(8, 8);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(320, 32);
			this.label16.TabIndex = 27;
			this.label16.Text = "Cálculo del inverso";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(24, 80);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(88, 23);
			this.label18.TabIndex = 25;
			this.label18.Text = "a";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// inversoA
			// 
			this.inversoA.BackColor = System.Drawing.SystemColors.Window;
			this.inversoA.Location = new System.Drawing.Point(120, 80);
			this.inversoA.Name = "inversoA";
			this.inversoA.Size = new System.Drawing.Size(112, 20);
			this.inversoA.TabIndex = 23;
			// 
			// logBox
			// 
			this.logBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.logBox.Location = new System.Drawing.Point(3, 281);
			this.logBox.Multiline = true;
			this.logBox.Name = "logBox";
			this.logBox.ReadOnly = true;
			this.logBox.Size = new System.Drawing.Size(344, 103);
			this.logBox.TabIndex = 2;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.logBox, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 109F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(350, 387);
			this.tableLayoutPanel1.TabIndex = 3;
			// 
			// label19
			// 
			this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.Location = new System.Drawing.Point(8, 8);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(320, 32);
			this.label19.TabIndex = 30;
			this.label19.Text = "Teorema chino de los restos";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// restosA1
			// 
			this.restosA1.Location = new System.Drawing.Point(96, 43);
			this.restosA1.Name = "restosA1";
			this.restosA1.Size = new System.Drawing.Size(54, 20);
			this.restosA1.TabIndex = 31;
			// 
			// restosA2
			// 
			this.restosA2.Location = new System.Drawing.Point(96, 69);
			this.restosA2.Name = "restosA2";
			this.restosA2.Size = new System.Drawing.Size(54, 20);
			this.restosA2.TabIndex = 32;
			// 
			// restosA3
			// 
			this.restosA3.Location = new System.Drawing.Point(96, 95);
			this.restosA3.Name = "restosA3";
			this.restosA3.Size = new System.Drawing.Size(54, 20);
			this.restosA3.TabIndex = 33;
			// 
			// restosA6
			// 
			this.restosA6.Location = new System.Drawing.Point(96, 174);
			this.restosA6.Name = "restosA6";
			this.restosA6.Size = new System.Drawing.Size(54, 20);
			this.restosA6.TabIndex = 36;
			// 
			// restosA5
			// 
			this.restosA5.Location = new System.Drawing.Point(96, 148);
			this.restosA5.Name = "restosA5";
			this.restosA5.Size = new System.Drawing.Size(54, 20);
			this.restosA5.TabIndex = 35;
			// 
			// restosA4
			// 
			this.restosA4.Location = new System.Drawing.Point(96, 122);
			this.restosA4.Name = "restosA4";
			this.restosA4.Size = new System.Drawing.Size(54, 20);
			this.restosA4.TabIndex = 34;
			// 
			// restosN6
			// 
			this.restosN6.Location = new System.Drawing.Point(199, 174);
			this.restosN6.Name = "restosN6";
			this.restosN6.Size = new System.Drawing.Size(54, 20);
			this.restosN6.TabIndex = 42;
			// 
			// restosN5
			// 
			this.restosN5.Location = new System.Drawing.Point(199, 148);
			this.restosN5.Name = "restosN5";
			this.restosN5.Size = new System.Drawing.Size(54, 20);
			this.restosN5.TabIndex = 41;
			// 
			// restosN4
			// 
			this.restosN4.Location = new System.Drawing.Point(199, 122);
			this.restosN4.Name = "restosN4";
			this.restosN4.Size = new System.Drawing.Size(54, 20);
			this.restosN4.TabIndex = 40;
			// 
			// restosN3
			// 
			this.restosN3.Location = new System.Drawing.Point(199, 95);
			this.restosN3.Name = "restosN3";
			this.restosN3.Size = new System.Drawing.Size(54, 20);
			this.restosN3.TabIndex = 39;
			// 
			// restosN2
			// 
			this.restosN2.Location = new System.Drawing.Point(199, 69);
			this.restosN2.Name = "restosN2";
			this.restosN2.Size = new System.Drawing.Size(54, 20);
			this.restosN2.TabIndex = 38;
			// 
			// restosN1
			// 
			this.restosN1.Location = new System.Drawing.Point(199, 43);
			this.restosN1.Name = "restosN1";
			this.restosN1.Size = new System.Drawing.Size(54, 20);
			this.restosN1.TabIndex = 37;
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(36, 43);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(54, 20);
			this.label17.TabIndex = 43;
			this.label17.Text = "a1";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(36, 69);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(54, 20);
			this.label20.TabIndex = 44;
			this.label20.Text = "a2";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(36, 121);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(54, 20);
			this.label21.TabIndex = 46;
			this.label21.Text = "a4";
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(36, 95);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(54, 20);
			this.label22.TabIndex = 45;
			this.label22.Text = "a3";
			this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(36, 173);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(54, 20);
			this.label23.TabIndex = 48;
			this.label23.Text = "a6";
			this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(36, 147);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(54, 20);
			this.label24.TabIndex = 47;
			this.label24.Text = "a5";
			this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label25
			// 
			this.label25.Location = new System.Drawing.Point(161, 173);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(32, 20);
			this.label25.TabIndex = 54;
			this.label25.Text = "n6";
			this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label26
			// 
			this.label26.Location = new System.Drawing.Point(161, 147);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(32, 20);
			this.label26.TabIndex = 53;
			this.label26.Text = "n5";
			this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label27
			// 
			this.label27.Location = new System.Drawing.Point(161, 121);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(32, 20);
			this.label27.TabIndex = 52;
			this.label27.Text = "n4";
			this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label28
			// 
			this.label28.Location = new System.Drawing.Point(161, 95);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(32, 20);
			this.label28.TabIndex = 51;
			this.label28.Text = "n3";
			this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label29
			// 
			this.label29.Location = new System.Drawing.Point(161, 69);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(32, 20);
			this.label29.TabIndex = 50;
			this.label29.Text = "n2";
			this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label30
			// 
			this.label30.Location = new System.Drawing.Point(161, 43);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(32, 20);
			this.label30.TabIndex = 49;
			this.label30.Text = "n1";
			this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// buttonRestos
			// 
			this.buttonRestos.Location = new System.Drawing.Point(112, 207);
			this.buttonRestos.Name = "buttonRestos";
			this.buttonRestos.Size = new System.Drawing.Size(112, 24);
			this.buttonRestos.TabIndex = 55;
			this.buttonRestos.Text = "Calcular";
			this.buttonRestos.UseVisualStyleBackColor = true;
			this.buttonRestos.Click += new System.EventHandler(this.ClickEventHandler);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(350, 387);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Criptografía";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			this.tabPage5.ResumeLayout(false);
			this.tabPage5.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}