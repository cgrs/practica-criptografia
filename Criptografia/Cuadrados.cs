﻿/*
 *
 * Usuario: cgrs
 * Fecha: 23/03/2017
 * Hora: 19:10
 * 
 */
using System;
using System.Numerics;
namespace Criptografia
{
	public class Cuadrados
	{
		public BigInteger b;
		
		public Cuadrados(BigInteger a, BigInteger e, BigInteger n)
		{
			b = BigInteger.ModPow(a,e,n);
		}
	}
}
